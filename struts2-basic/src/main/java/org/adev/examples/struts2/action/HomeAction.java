package org.adev.examples.struts2.action;

import java.util.Arrays;

import org.adev.examples.struts2.bean.Profile;

import com.opensymphony.xwork2.ActionSupport;

public class HomeAction extends ActionSupport {
	
	private Profile profile;
	
	private static final long serialVersionUID = -6129036176071791070L;

	@Override
	public String execute() throws Exception {
		profile = new Profile();
		
		profile.setName("Juan");
		profile.setAge(29);
		profile.setUsername("jgarcia");
		profile.setRoles(Arrays.asList(new String[] {
			"USER",
			"ADMIN"
		}));
		
		return SUCCESS;
	}

	public Profile getProfile() {
		return profile;
	}
	
}
