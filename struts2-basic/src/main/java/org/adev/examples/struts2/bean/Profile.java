package org.adev.examples.struts2.bean;

import java.io.Serializable;
import java.util.List;

public class Profile implements Serializable {

	private String			name;
	private String			username;
	private Integer			age;
	private List<String>	roles;
	
	private static final long serialVersionUID = 1346735313384491007L;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
}
