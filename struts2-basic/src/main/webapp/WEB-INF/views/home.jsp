<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html>
<html>
  <head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
  </head>
  <body>
    <h1>Hello From Struts 2 Action</h1>
    <s:form>
      <s:textfield label="Name" key="profile.name" />
      <s:textfield label="Alias" key="profile.username" />
      <s:textfield label="Age" key="profile.age" />
      <s:combobox id="role" name="cmbRole" list="profile.roles" headerKey="" headerValue="--- Select ---" />
    </s:form>
  </body>
</html>